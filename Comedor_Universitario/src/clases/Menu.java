package clases;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;

@PersistenceCapable
public class Menu {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idMenu;	
	@Persistent
	private int voto;
	@Persistent
	private int estado;
	@Persistent
	private String nombre;
		
	@Persistent
	@Unowned
	protected Plato plato;

	public Menu(int voto, int estado) {
		super();
		this.voto = voto;
		this.estado = estado;
		this.plato =null;
		this.nombre = null;
	}
	
	public String getIdMenu() {
		return KeyFactory.keyToString(idMenu);
	}
	public int getVoto() {
		return voto;
	}
	public String getNameEstado(){
	
		switch(this.estado){
		case 1: return "Activo";
		case 2: return "Inactivo";
		case 3: return "Pendiente";
		default: return "";
		}
		
	}

	public String getName() {
		return plato.getNombre();
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public Plato getPlato() {
		return plato;
	}

	public void setPlato(Plato plato) {
		this.plato = new Plato(plato);
		this.nombre=plato.getNombre();
	}	
	
	public String getDescripcion(){
		return plato.getDescripcion();
	}
	
	public String getFoto(){
		return plato.getFoto();
	}

	
	
}
