
package clases;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;

@PersistenceCapable
public class Nutricionista {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idPensionista;	

	@Persistent
	private String comida1;
	@Persistent
	private String comida2;
	@Persistent
	private String comida3;
	@Persistent
	private String comida4;
	public Nutricionista(){
		comida1="Nada";
		comida2="Nada";
		comida3="Nada";
		comida4="Nada";
	
	}

	


	public Nutricionista(String comida1, String comida2, String comida3,
			String comida4 ) {
		
		this.comida1 = comida1;
		this.comida2 = comida2;
		this.comida3 = comida3;
		this.comida4 = comida4;
	
	}
	

	

	public String getComida1() {
		return comida1;
	}
	
	public String getComida2() {
		return comida2;
	}
	
	public String getComida3(){
		return comida3;
		}
		
	
	
	public String getComida4() {
		return comida4;
	}

	public void setComida1(String comida1) {
		this.comida1 = comida1;
	}
	public void setComida2(String comida2) {
		this.comida2 = comida2;
	}
	public void setComida3(String comida3) {
		this.comida3 = comida3;
	}
	public void setComida4(String comida4) {
		this.comida4 = comida4;
	}

	
	
}
