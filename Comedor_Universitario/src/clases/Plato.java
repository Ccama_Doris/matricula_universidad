package clases;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable
public class Plato {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idPlato;
	@Persistent 
	private String nombre;
	@Persistent 
	private String descripcion;
	@Persistent 
	private String foto;
	@Persistent
	private int estado;
	
	
	public Plato(Plato plato){
		this.idPlato = plato.getId();
		this.nombre = plato.getNombre();
		this.descripcion = plato.getDescripcion();
		this.foto = plato.getFoto();
		this.estado = plato.getEstado();
	}
	
	public Plato(String nombre, String descripcion, String foto, int estado) {
		this.nombre = nombre;
		this.descripcion = descripcion;
		this.foto = foto;
		this.estado = estado;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public String getDescripcion() {
		return descripcion;
	}

	public void setDescripcion(String descripcion) {
		this.descripcion = descripcion;
	}

	public String getFoto() {
		return foto;
	}

	public void setFoto(String foto) {
		this.foto = foto;
	}
	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getIdPlato() {
		return KeyFactory.keyToString(idPlato);
	}
	
	public Key getId() {
		return (idPlato);
	}

	
}
