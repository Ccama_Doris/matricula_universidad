package clases;

import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@PersistenceCapable
public class Usuario {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idUsuario;
	@Persistent 
	private String cui;
	@Persistent 
	private String name;
	@Persistent 
	private String pat;
	@Persistent 
	private String mat;
	@Persistent
	private String dni;
	@Persistent
	private int estado;
	
	public Usuario(Usuario user){
		this.idUsuario = user.getId();
		this.cui = user.getCui();
		this.name = user.getName();
		this.pat = user.getPat();
		this.mat = user.getMat();
		this.dni = user.getDni();
		this.estado = user.getEstado();
	}
	
	public Usuario(String cui, String name, String pat, String mat, String dni,int estado) {
		super();
		this.cui = cui;
		this.name = name;
		this.pat = pat;
		this.mat = mat;
		this.dni = dni;
		this.estado = estado;
	}

	public String getCui() {
		return cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getPat() {
		return pat;
	}

	public void setPat(String pat) {
		this.pat = pat;
	}

	public String getMat() {
		return mat;
	}

	public void setMat(String mat) {
		this.mat = mat;
	}
	
	public String getDni() {
		return dni;
	}

	public void setDni(String dni) {
		this.dni = dni;
	}

	public int getEstado() {
		return estado;
	}

	public void setEstado(int estado) {
		this.estado = estado;
	}

	public String getIdUsuario() {
		return KeyFactory.keyToString(idUsuario);
	}
	
	public Key getId() {
		return (idUsuario);
	}

	public String getApellidos() {
		return this.pat + " "+this.mat;
	}

	
}
