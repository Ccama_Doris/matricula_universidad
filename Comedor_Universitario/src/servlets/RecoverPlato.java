package servlets;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.jdo.Transaction;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import clases.*;

@SuppressWarnings("serial")
public class RecoverPlato extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {

		resp.setContentType("text/html");

		String clave = (req.getParameter("clave"));
		Key key = KeyFactory.stringToKey(clave);
		

		final PersistenceManager pm = PMF.get().getPersistenceManager();
		Plato found = pm.getObjectById(Plato.class, key);
		
		PrintWriter out = resp.getWriter();
			
		out.println("<form>");
		out.println("<input type='hidden' value='"+clave+ "' id='idPlato' />");

		out.println("<div class='form-group'>");
		out.println("<label for='nombre'><span class='glyphicon glyphicon-tag'></span> Nombre del Plato</label>");
		out.println("<input type='text' class='form-control' id='nombreUpd' value='"+found.getNombre()+"' placeholder='Ingrese nombre del plato' required>");
		out.println("</div>");
		out.println("<div class='form-group'>");
		out.println("<label for='descripcion'><span class='glyphicon glyphicon-tag'></span> Descripcion</label>");
		out.println("<input type='text' class='form-control' id='descripcionUpd' value='"+found.getDescripcion()+"' placeholder='Ingrese breve Descripcion' required>");
		out.println("</div>");
		out.println("<div class='form-group'>");
		out.println("<label for='foto'><span class='glyphicon glyphicon-tag'></span> Foto</label>");
		out.println("<input type='text' class='form-control' id='fotoUpd' value='"+found.getFoto()+"' placeholder='Ingrese Foto' required>");
		out.println("</div>");
		
		out.println("<button type='submit' id='updPla' class='btn btn-success btn-block'><span class='glyphicon glyphicon-floppy-disk'></span> Modificar</button>");
		out.println("</form>");
				
	}
}
