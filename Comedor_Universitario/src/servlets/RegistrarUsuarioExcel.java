package servlets;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import clases.PMF;
import clases.Usuario;
import jxl.Cell;
import jxl.Sheet;
import jxl.Workbook;
import jxl.read.biff.BiffException;

@SuppressWarnings("serial")

public class RegistrarUsuarioExcel extends HttpServlet {

	protected void doGet(HttpServletRequest req,
			HttpServletResponse resp) throws ServletException, IOException {
			resp.setContentType("text/html");
			//FilePermission perm = new FilePermission("C:\\Users\\Corei7\\Desktop\\data.xls", "read,execute");

		try{
			//File f = new File("/data.xls");	
			File f = new File("C:\\Users\\Corei7\\Desktop\\Vicki Presentacion running\\Comedor_Universitario\\srcdata.xls");
			Workbook wb = Workbook.getWorkbook(f);
			Sheet s = wb.getSheet(0);
			int row = s.getRows();
			int col = s.getColumns();
			
			List<Usuario> users = new ArrayList<Usuario>();		
			
			for (int i = 1; i < row; i++){
				Cell c = s.getCell(0,i);
				Cell d = s.getCell(1,i);
				Cell p = s.getCell(2,i);
				Cell m = s.getCell(3,i);
				Cell n = s.getCell(4,i);
				
			
				
				Usuario u = new Usuario(c.getContents(),n.getContents(),p.getContents(),m.getContents(),d.getContents(),1);
				PersistenceManager pm = PMF.get().getPersistenceManager();
				
				try{
					pm.makePersistent(users);
					
					resp.getWriter().println("saving ...");			
			
				}catch(Exception e){
					System.out.println(e);
					resp.getWriter().println("Ocurrio un error, <a href='index.html'>vuelva a intentarlo</a>");
				}finally{
					pm.close();
				}
			}
			System.out.println(users.size());
		}
	 catch (BiffException e1) {
		// TODO Auto-generated catch block
		e1.printStackTrace();
	}
		}
}

