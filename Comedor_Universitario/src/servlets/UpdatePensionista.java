package servlets;
import java.io.IOException;

import javax.jdo.PersistenceManager;
import javax.jdo.Transaction;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

import clases.*;

@SuppressWarnings("serial")
public class UpdatePensionista extends HttpServlet {
	public void doGet(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		
		resp.setContentType("text/html");	
		
		String escuela = req.getParameter("escuelaUpd");		
		String email = req.getParameter("emailUpd");
		String telefono = req.getParameter("numberUpd");
		String pass = req.getParameter("passUpd");	
		
		String idPensionista = req.getParameter("idPensionista");
		
		Key kPensionista = (Key)KeyFactory.stringToKey((String)idPensionista);
		
		final PersistenceManager pm = PMF.get().getPersistenceManager();

		Transaction tx = pm.currentTransaction();
		tx.begin();
		
		try{
			
			Pensionista foundPensionista = pm.getObjectById(Pensionista.class, kPensionista);
			Pensionista p = new Pensionista(escuela,email,telefono, pass,1);	
			
			try{
				foundPensionista.setEscuela(p.getEscuela());
				foundPensionista.setEmail(p.getEmail());
				foundPensionista.setTelefono(p.getTelefono());
				foundPensionista.setPassword(p.getPassword());
			
				
				tx.commit();
				resp.getWriter().println("Pensionista modificado correctamente.");
			}catch(Exception e){
				System.out.println(e);
				resp.sendRedirect("/pensionistaServlet");
			}
			
		}catch(Exception e){
			System.out.println(e);
		}finally{
			try {
				if (tx.isActive())
					tx.rollback();
            } finally {
            	pm.close();
            }
		}
	}
}