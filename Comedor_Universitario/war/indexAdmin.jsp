<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="servlets.*"%>
<%@ page import="java.util.List"%>

<%
	List<Pensionista> pensionistas = (List<Pensionista>) request.getAttribute("pensionistas");
	List<Usuario> usuarios = (List<Usuario>) request.getAttribute("usuarios");
%>

<%HttpSession misesion= request.getSession(); %>
<%if(misesion.getAttribute("cui") == null){ 			
				response.sendRedirect("index.html");
	}
else {
%>


<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />






<title>Comedor Universitario</title>

<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css" />
<link href="css/nivo-lightbox.css" rel="stylesheet" />
<link href="css/nivo-lightbox-theme/default/default.css"
	rel="stylesheet" type="text/css" />
<link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
<link href="css/owl.theme.css" rel="stylesheet" media="screen" />
<link href="css/flexslider.css" rel="stylesheet" />
<link href="css/animate.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="color/default.css" rel="stylesheet">

<link href="css/main/buscarUsuario.css" rel="stylesheet">
<link href="css/main/miStyle.css" rel="stylesheet"/>



</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">



	<!-- Navigation -->
	<div id="navigation">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="site-logo">
							<a href="index.html" class="brand">Bienvenido Administrador</a>
						</div>
					</div>


					<div class="col-md-10">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target="#menu">
								<i class="fa fa-bars"></i>
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="menu">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="#table_usuarios">Usuarios</a></li>
								<li><a href="#table_pensionistas">Pensionistas</a></li>
								<li class="active"><a href="cerrarSesion">Salir</a></li>
							</ul>
						</div>
						<!-- /.Navbar-collapse -->

					</div>
				</div>
			</div>
			<!-- /.container -->
		</nav>
	</div>
	<!-- /Navigation -->

	<!-- CRUD USUARIOS -->


	<section id="table_usuarios" class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
						<div class="section-heading text-center">
							<h2 class="h-bold">Usuarios</h2>
							<div class="divider-header"></div>
							<p>Lista de todos los Alumnos aceptados en el Comedor.</p>
						
						</div>
					</div>
				</div>
			</div>
			<div class="row wow bounceInUp" data-wow-offset="10" data-wow-delay="0.2s" >

				
				<div class="form-group pull-right ">
					<input type="text" class="search form-control" placeholder="Busque un Usuario">
				</div>
				<div class="row">
					<span class="col-md-3 col-sm-12 col-xs-12">Subir archivo Excel
					    <button type="submit" class="btn btn-info pull-left" id="upExcel"><span class="glyphicon glyphicon-cloud-upload"></span></button> 
					</span>
					<span class="col-md-3 col-sm-12 col-xs-12">Agregar Nuevo Usuario
					   
					    <button type="submit" class="btn btn-info pull-left" id="agregarUsuario"><span class="glyphicon glyphicon-plus-sign"></span></button> 
					   

					</span>
					<span class="col-md-3 col-sm-12	col-xs-12">Borrar Todos los Usuarios
					  <a class="btn btn-info pull-left" type="btn" data-toggle="modal"
							data-target="#modal_eliminarAllUser"><span class="glyphicon glyphicon-trash"></span></a> 
					</span>

				</div>
				<!-- tabla usuarios -->
				<span class="counter pull-right"></span>
				<table class="table table-hover table-bordered results" >
					<thead>
						<tr>
							<th>Nro</th>
						    <th class="col-md-2 col-xs-2">CUI</th>
						    <th class="col-md-2 col-xs-2">DNI</th>
						    <th class="col-md-3 col-xs-3">Apellidos</th>
						    <th class="col-md-3 col-xs-3">Nombre</th>
						    <th colspan="2" scope="colgrup" class="col-md-2 col-xs-2">Accion</th>

						</tr>
						<tr class="warning no-result">
						      <td colspan="6"><i class="fa fa-warning"></i> Usuario NO Encontrado</td>
						</tr>
					</thead>
					<tbody id="tablaUsuario" >
						
							<%
								if (usuarios != null) {
									int i = 1;
									for (Usuario u : usuarios) {
							%>
										<tr>
											<th scope="row"><%=i%></th>
											<td><%=u.getCui()%></td>
											<td><%=u.getDni()%></td>
											<td><%=u.getApellidos()%></td>
											<td><%=u.getName()%></td>

											<td><button type="submit" class="btn btn-success center-block updUsuario" value='<%=u.getIdUsuario() %>'><span class="glyphicon glyphicon-pencil"></span></button></td>	

											<td><button type="submit" class="btn btn-danger center-block" onclick="deleteItem(<%=u.getCui()%>);"><span class="glyphicon glyphicon-trash"></span></button></td>
										</tr>
							<%
									i++;
									}
								}
							%>


					</tbody>
				</table>
				<!-- /tabla usuarios -->
				<a href="https://youtu.be/72uSgPysqrY">Video Tutorial</a>
			</div>
		</div>
		

	</section>

 			<!-- Modal Crear Usuario -->
					<div class="modal fade" id="myModal" role="dialog">
					    <div class="modal-dialog">
							    
					<!-- Modal content-->
						    <div class="modal-content">
						        <div class="modal-header" style="padding:35px 50px;">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4><span class="glyphicon glyphicon-user"></span> Nuevo Usuario</h4>
						        </div>
						        <div class="modal-body" style="padding:40px 50px;">
							      	<form>
							            <div class="form-group">
								            <label for="cui"><span class="glyphicon glyphicon-tag"></span> CUI</label>
							    	        <input type="number" class="form-control" id="cui" placeholder="Ingrese codigo del alumno" required>
							            </div>
							            <div class="form-group">
							        	    <label for="dni"><span class="glyphicon glyphicon-tag"></span> DNI</label>
							            	<input type="number" class="form-control" id="dni" placeholder="Ingrese Documento de Identidad" required>
							            </div>
							            <div class="form-group">
							        	    <label for="apellidop"><span class="glyphicon glyphicon-tag"></span> Apellido Paterno</label>
							            	<input type="text" class="form-control" id="apaterno" placeholder="Ingrese Apellido Paterno" required>
							            </div>
							            <div class="form-group">
							        	    <label for="apellidom"><span class="glyphicon glyphicon-tag"></span> Apellido Materno</label>
							            	<input type="text" class="form-control" id="amaterno" placeholder="Ingrese Apellido Materno" required>
							            </div>
							            <div class="form-group">
							        	    <label for="nombre"><span class="glyphicon glyphicon-tag"></span> Nombre</label>
							            	<input type="text" class="form-control" id="name" placeholder="Ingrese Nombre" required>
							            </div>
							              <button type="submit" id="registrar" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-floppy-disk"></span> Agregar</button>
							      	</form>
							    </div>
					<!-- Modal footer-->
		    
							    <div class="modal-footer">
							        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							       
							    </div>
							</div>
							      
						</div>
					</div>

			<!-- /Modal crear Usuario-->



			<!-- Modal Modificar Usuario -->
					<div class="modal fade" id="myModalUpd" role="dialog">
					    <div class="modal-dialog">
							    
					<!-- Modal content-->
						    <div class="modal-content">
						        <div class="modal-header" style="padding:35px 50px;">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4><span class="glyphicon glyphicon-pencil"></span> Modificar Usuario</h4>
						        </div>

						        <div class="modal-body" id="update_usuario" style="padding:40px 50px;">
							    

							    
							    </div>
					<!-- Modal footer-->
		    
							    <div class="modal-footer">
							        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							       
							    </div>
							</div>
							      
						</div>
					</div>

			<!-- /Modal modificar Usuario-->

			<!-- Modal Delete All Usuarios -->

			<div class="modal fade" id="modal_eliminarAllUser" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Eliminar</h4>
						</div>
						<div class="modal-body">
							<p>Esta Seguro de Borrar todos los Usuarios? (Se borraran tambien los Pensionistas)</p>
						</div>
						<div class="modal-footer">
							<button type="button" data-dismiss="modal">Cancelar</button>
							<button type="button" data-dismiss="modal" id="deleteAllUsers">Eliminar</button>
						</div>
					</div>
				</div>
			</div>

		<!-- Fin Modal Delete All Usuarios -->


	<!-- /CRUD USUARIOS -->


	


	<!-- CRUD PENSIONISTAS -->

	<section id="table_pensionistas" class="home-section nopadd-bot color-dark bg-gray text-center">
			<div class="container">
				<div class="row">
					<div class="col-lg-8 col-lg-offset-2">
						<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
							<div class="section-heading text-center">
								<h2 class="h-bold">Pensionistas</h2>
								<div class="divider-header"></div>
								<p>Lista de todos los Pensionistas registrados en la pagina.</p>
							</div>
						</div>
					</div>
				</div>
				<div class="row wow bounceInUp" data-wow-offset="10" data-wow-delay="0.2s" >
					<div class="form-group pull-right ">
						<input type="text" class="searchP form-control" placeholder="Busque un Pensionista">
					</div>
					<div class="row">
						<span class="col-md-3 col-sm-12	col-xs-12">Borrar Todos los Pensionistas
						  <a class="btn btn-info pull-left" type="btn" data-toggle="modal"
							data-target="#modal_eliminarAllPensionistas"><span class="glyphicon glyphicon-trash"></span></a>
						</span>
					</div>
					<!-- tabla pensionistas -->
					<span class="counter pull-right"></span>
					<table class="table table-hover table-bordered results" >
						<thead>
							<tr>
								<th>Nro</th>
							    <th class="col-md-1 col-xs-1">CUI</th>
							    <th class="col-md-1 hidden-xs">Nombre</th>
							    <th class="col-md-2 col-xs-4">Apellidos</th>
							    <th class="col-md-2 hidden-xs">Escuela</th>
							    <th class="col-md-1 hidden-xs">DNI</th>
							    <th class="col-md-1 hidden-xs">E-mail</th>
							    <th class="hidden-md hidden-sm hidden-xs">Telefono</th>
							    <th class="hidden-md hidden-sm col-xs-2">Password</th>
							    <th class="col-md-1 col-xs-2">Check</th>
							    <th class="col-md-1 col-xs-2">Modificar</th>

							</tr>
							<tr class="warning no-result">
							      <td colspan="9"><i class="fa fa-warning"></i> Pensionista NO Encontrado</td>
							</tr>
						</thead>
						<tbody id="table_Pensionista" >
							
								<%
									if (pensionistas != null) {
										int i = 1;
										for (Pensionista p : pensionistas) {
								%>
											<tr>
												<th scope="row"><%=i%></th>
												<td><%=p.getCui()%></td>
												<td class="hidden-xs"><%=p.getName()%></td>
												<td><%=p.getApellidos()%></td>
												<td class="hidden-xs"><%=p.getEscuela()%></td>
												<td class="hidden-xs"><%=p.getDni()%></td>
												<td class="hidden-xs"><%=p.getEmail()%></td>
												<td class="hidden-md hidden-sm hidden-xs"><%=p.getTelefono()%></td>
												<td class="hidden-md hidden-sm"><%=p.getPassword()%></td>

												<td><input type="checkbox" name="option[]" id="option[] "
														value='<%=p.getIdPensionista()%>' /></td>	

												<td><button type="submit" class="btn btn-success center-block updPensionista" value='<%=p.getIdPensionista() %>'><span class="glyphicon glyphicon-pencil"></span></button></td>

												
											</tr>
								<%
										i++;
										}
									}
								%>
						</tbody>
					</table>
					<!-- /tabla pensionistas -->
					<div class="col-md-2 col-md-offset-2"></div>
					<div class="row marginbot-80">
						<div class="col-md-2 col-md-offset-2"></div>
						<div class="col-md-8 col-md-offset-2">						
							<button  class="btn btn-danger" type="btn" data-toggle="modal"
							data-target="#modal_eliminar">Eliminar</button>
						</div>
						<div class="col-md-2 col-md-offset-2"></div>
					</div>
					<div class="fila"></div>

				</div>
				
			</div>
	</section>


			<!-- Modal Modificar Pensionista -->
					<div class="modal fade" id="myModalUpdP" role="dialog">
					    <div class="modal-dialog">
							    
					<!-- Modal content-->
						    <div class="modal-content">
						        <div class="modal-header" style="padding:35px 50px;">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4><span class="glyphicon glyphicon-pencil"></span> Modificar Pensionista</h4>
						        </div>

						        <div class="modal-body" id="update_pensionista" style="padding:40px 50px;">
							    

							    
							    </div>
					<!-- Modal footer-->
		    
							    <div class="modal-footer">
							        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							       
							    </div>
							</div>
							      
						</div>
					</div>

			<!-- /Modal modificar Usuario-->

	<!-- /CRUD PENSIONISTAS -->


	<!-- Section: contact -->

	<section>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">

						<div class="text-center">
							<a href="#estadistica" class="totop"><i
								class="fa fa-angle-up fa-3x"></i></a>

							<p>Universidad Nacional de San Agustin, Arequipa, Peru <br/>
							&copy;Copyright 2016 - Shuffle. Designed by <a href="http://facebook.com">Virgilia, Yesica, Mauricio y Gerson</a></p>
	                                              

						</div>
					</div>
				</div>
			</div>
		</footer>
	</section>




	<!-- Core JavaScript Files -->

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
	<script src="js/jquery.easing.min.js"></script>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>

	<script src="js/custom.js"></script>

	<script src="js/main/buscarUsuario.js"></script>
	<script src="js/main/mainAdmin.js"></script>




	<!-- Modal Delete Pensionista -->

	<div class="modal fade" id="modal_eliminar" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Eliminar</h4>
				</div>
				<div class="modal-body">
					<p>Esta Seguro de Borrar?</p>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal">Cancelar</button>
					<button type="button" data-dismiss="modal" id="_eliminar">Eliminar</button>
				</div>
			</div>
		</div>
	</div>


	<!-- fin delete Pensionista -->

	<!-- Modal Delete AllPensionistas -->

	<div class="modal fade" id="modal_eliminarAllPensionistas" role="dialog">
		<div class="modal-dialog">
			<!-- Modal content-->
			<div class="modal-content">
				<div class="modal-header">
					<button type="button" class="close" data-dismiss="modal">&times;</button>
					<h4 class="modal-title">Eliminar</h4>
				</div>
				<div class="modal-body">
					<p>Esta Seguro de Borrar todos los Pensionistas?</p>
				</div>
				<div class="modal-footer">
					<button type="button" data-dismiss="modal">Cancelar</button>
					<button type="button" data-dismiss="modal" id="delAllPens">Eliminar</button>
				</div>
			</div>
		</div>
	</div>


	<!-- fin delete AllPensionistas -->


</body>

</html>
<%} %>