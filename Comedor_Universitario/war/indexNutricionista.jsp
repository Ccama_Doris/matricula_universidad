<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="servlets.*"%>
<%@ page import="java.util.List"%>

<%
	List<Plato> platos = (List<Plato>) request.getAttribute("platos");
%>

<!DOCTYPE html>
<html lang="en">

<head>
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<meta name="description" content="">
<meta name="author" content="">
<meta charset="utf-8">
<meta name="viewport" content="width=device-width, initial-scale=1" />






<title>Comedor Universitario</title>

<!-- css -->
<link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
<link href="font-awesome/css/font-awesome.min.css" rel="stylesheet"
	type="text/css" />
<link href="css/nivo-lightbox.css" rel="stylesheet" />
<link href="css/nivo-lightbox-theme/default/default.css"
	rel="stylesheet" type="text/css" />
<link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
<link href="css/owl.theme.css" rel="stylesheet" media="screen" />
<link href="css/flexslider.css" rel="stylesheet" />
<link href="css/animate.css" rel="stylesheet" />
<link href="css/style.css" rel="stylesheet" />
<link href="color/default.css" rel="stylesheet">

<link href="css/main/buscarUsuario.css" rel="stylesheet">
<link href="css/main/miStyle.css" rel="stylesheet"/>



</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">



	<!-- Navigation -->
	<div id="navigation">
		<nav class="navbar navbar-custom" role="navigation">
			<div class="container">
				<div class="row">
					<div class="col-md-2">
						<div class="site-logo">
							<a href="index.html" class="brand">Bienvenido Nutricionista</a>
						</div>
					</div>


					<div class="col-md-10">

						<!-- Brand and toggle get grouped for better mobile display -->
						<div class="navbar-header">
							<button type="button" class="navbar-toggle"
								data-toggle="collapse" data-target="#menu">
								<i class="fa fa-bars"></i>
							</button>
						</div>
						<!-- Collect the nav links, forms, and other content for toggling -->
						<div class="collapse navbar-collapse" id="menu">
							<ul class="nav navbar-nav navbar-right">
								<li><a href="#table_platos">Platos</a></li>
								<li><a href="#table_menus">Menu</a></li>
								<li class="active"><a href="cerrarSesion">Salir</a></li>
							</ul>
						</div>
						<!-- /.Navbar-collapse -->

					</div>
				</div>
			</div>
			<!-- /.container -->
		</nav>
	</div>
	<!-- /Navigation -->

	<!-- CRUD USUARIOS -->


	<section id="table_platos" class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
						<div class="section-heading text-center">
							<h2 class="h-bold">Platos</h2>
							<div class="divider-header"></div>
							<p>Lista de Platos para este dia.</p>
						
						</div>
					</div>
				</div>
			</div>
			<div class="row wow bounceInUp" data-wow-offset="10" data-wow-delay="0.2s" >

				
				<div class="form-group pull-right ">
					<input type="text" class="search form-control" placeholder="Busque un Usuario">
				</div>
				<div class="row">
					<span class="col-md-3 col-sm-12 col-xs-12">Agregar Nuevo Plato
					   
					    <button type="submit" class="btn btn-info pull-left" id="agregarPlato"><span class="glyphicon glyphicon-plus-sign"></span></button> 
					   

					</span>
					
				</div>
				<!-- tabla usuarios -->
				<span class="counter pull-right"></span>
				<table class="table table-hover table-bordered PARABUSCARSEARCHresults" >
					<thead>
						<tr>
							<th>Nro</th>
						    <th class="col-md-2 col-xs-2">Nombre</th>
						    <th class="col-md-4 col-xs-4">Descripcion</th>
						    <th class="col-md-4 col-xs-4">Foto</th>
						    <th colspan="2" scope="colgrup" class="col-md-2 col-xs-2">Accion</th>

						</tr>
						<tr class="warning no-result">
						      <td colspan="6"><i class="fa fa-warning"></i> Plato NO Encontrado</td>
						</tr>
					</thead>
					<tbody id="FALTAIMPLEMENTARtablaUsuario" >
						
							<%
								if (platos != null) {
									int i = 1;
									for (Plato p : platos) {
							%>
										<tr>
											<th scope="row"><%=i%></th>
											<td><%=p.getNombre()%></td>
											<td><%=p.getDescripcion()%></td>
											<td><%=p.getFoto()%></td>

											<td><button type="submit" class="btn btn-success center-block updPlato" value='<%=p.getIdPlato() %>'><span class="glyphicon glyphicon-pencil"></span></button></td>	

																		
										</tr>
							<%
									i++;
									}
								}
							%>


					</tbody>
				</table>
				<!-- /tabla usuarios -->

			</div>
		</div>
		

	</section>

 			<!-- Modal Crear Plato -->
					<div class="modal fade" id="myModalPlato" role="dialog">
					    <div class="modal-dialog">
							    
					<!-- Modal content-->
						    <div class="modal-content">
						        <div class="modal-header" style="padding:35px 50px;">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4><span class="glyphicon glyphicon-user"></span> Nuevo Plato</h4>
						        </div>
						        <div class="modal-body" style="padding:40px 50px;">
							      	<form>
							            <div class="form-group">
								            <label for="nombre"><span class="glyphicon glyphicon-tag"></span> Nombre del Plato</label>
							    	        <input type="text" class="form-control" id="nombre" placeholder="Ingrese nombre del plato" required>
							            </div>
							            <div class="form-group">
							        	    <label for="descripcion"><span class="glyphicon glyphicon-tag"></span> Descripcion</label>
							            	<input type="text" class="form-control" id="descripcion" placeholder="Ingrese breve descripcion" required>
							            </div>
							            <div class="form-group">
							        	    <label for="foto"><span class="glyphicon glyphicon-tag"></span> Foto</label>
							            	<input type="text" class="form-control" id="foto" placeholder="Ingrese Foto" required>
							            </div>
							           							            
							              <button type="submit" id="registrar" class="btn btn-primary btn-block"><span class="glyphicon glyphicon-floppy-disk"></span> Agregar</button>
							      	</form>
							    </div>
					<!-- Modal footer-->
		    
							    <div class="modal-footer">
							        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							       
							    </div>
							</div>
							      
						</div>
					</div>

			<!-- /Modal crear Usuario-->



			<!-- Modal Modificar Plato -->
					<div class="modal fade" id="myModalUpdt" role="dialog">
					    <div class="modal-dialog">
							    
					<!-- Modal content-->
						    <div class="modal-content">
						        <div class="modal-header" style="padding:35px 50px;">
							          <button type="button" class="close" data-dismiss="modal">&times;</button>
							          <h4><span class="glyphicon glyphicon-pencil"></span> Modificar Usuario</h4>
						        </div>

						        <div class="modal-body" id="update_plato" style="padding:40px 50px;">
							    

							    
							    </div>
					<!-- Modal footer-->
		    
							    <div class="modal-footer">
							        <button type="submit" class="btn btn-danger btn-default pull-left" data-dismiss="modal"><span class="glyphicon glyphicon-remove"></span> Cancel</button>
							       
							    </div>
							</div>
							      
						</div>
					</div>

			<!-- /Modal modificar Plato-->

			<!-- Modal Delete All Usuarios -->

			<div class="modal fade" id="modal_eliminarAllPlatos" role="dialog">
				<div class="modal-dialog">
					<!-- Modal content-->
					<div class="modal-content">
						<div class="modal-header">
							<button type="button" class="close" data-dismiss="modal">&times;</button>
							<h4 class="modal-title">Eliminar</h4>
						</div>
						<div class="modal-body">
							<p>Esta Seguro de Borrar todos los Platos? (Se borraran tambien los Menus)</p>
						</div>
						<div class="modal-footer">
							<button type="button" data-dismiss="modal">Cancelar</button>
							<button type="button" data-dismiss="modal" id="FALTAENVIARIDdeleteAllUsers">Eliminar</button>
						</div>
					</div>
				</div>
			</div>

		<!-- Fin Modal Delete All Usuarios -->


	<!-- /CRUD USUARIOS -->


	


	<!-- Section: contact -->

	<section>
		<footer>
			<div class="container">
				<div class="row">
					<div class="col-md-6 col-md-offset-3">

						<div class="text-center">
							<a href="#estadistica" class="totop"><i
								class="fa fa-angle-up fa-3x"></i></a>

							<p>Universidad Nacional de San Agustin, Arequipa, Peru <br/>
							&copy;Copyright 2016 - Shuffle. Designed by <a href="http://facebook.com">Virgilia, Yesica, Mauricio y Gerson</a></p>
	                                              

						</div>
					</div>
				</div>
			</div>
		</footer>
	</section>




	<!-- Core JavaScript Files -->

	<script src="js/jquery.min.js"></script>
	<script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
	<script src="js/jquery.easing.min.js"></script>
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>

	<script src="js/custom.js"></script>

	<script src="js/main/buscarUsuario.js"></script>
	<script src="js/main/mainNutri.js"></script>





</body>

</html>