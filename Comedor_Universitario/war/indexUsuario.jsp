<%@ page language="java" contentType="text/html; charset=ISO-8859-1"
	pageEncoding="ISO-8859-1"%>
<%@ page import="clases.*"%>
<%@ page import="java.util.List"%>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreServiceFactory" %>
<%@ page import="com.google.appengine.api.blobstore.BlobKey" %>
<%@ page import="com.google.appengine.api.blobstore.BlobstoreService" %>



<%HttpSession misesion= request.getSession(); %>
<%if(misesion.getAttribute("cui") == null){ 			
				response.sendRedirect("index.html");
	}
else {
%>

<%
    BlobstoreService blobstoreService = BlobstoreServiceFactory.getBlobstoreService();
%>
<%
	List<Nutricionista> nutricionistas = (List<Nutricionista>) request.getAttribute("nutricionistas");
int comida1 = 0, comida2 = 0, comida3 = 0, comida4 = 0;
Nutricionista nab=new Nutricionista("papa a la huancaina","caldo blanco","americano","rocoto relleno");
%>
<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="Pagina de consulta del usuario">
    <meta name="author" content="">
	
    <title>Comedor Universitario</title>
	
    <!-- css -->
    <link href="css/bootstrap.min.css" rel="stylesheet" type="text/css">
    <link href="font-awesome/css/font-awesome.min.css" rel="stylesheet" type="text/css" />
	<link href="css/nivo-lightbox.css" rel="stylesheet" />
	<link href="css/nivo-lightbox-theme/default/default.css" rel="stylesheet" type="text/css" />
	<link href="css/owl.carousel.css" rel="stylesheet" media="screen" />
    <link href="css/owl.theme.css" rel="stylesheet" media="screen" />
	<link href="css/flexslider.css" rel="stylesheet" />
	<link href="css/animate.css" rel="stylesheet" />
    <link href="css/style.css" rel="stylesheet">
	<link href="color/default.css" rel="stylesheet">
	
	
	    <!-- Core JavaScript Files -->
    <script src="js/jquery.min.js"></script>	 
    <script src="js/bootstrap.min.js"></script>
	<script src="js/jquery.sticky.js"></script>
	<script src="js/jquery.flexslider-min.js"></script>
    <script src="js/jquery.easing.min.js"></script>	
	<script src="js/jquery.scrollTo.js"></script>
	<script src="js/jquery.appear.js"></script>
	<script src="js/stellar.js"></script>
	<script src="js/wow.min.js"></script>
	<script src="js/owl.carousel.min.js"></script>
	<script src="js/nivo-lightbox.min.js"></script>

    <script src="js/custom.js"></script>
    
    
	<style>
	table {
    border: 1px solid black;
    border-collapse: collapse;
}
	th, td {
    padding: 5px;
    text-align:center;
}
</style>

</head>

<body id="page-top" data-spy="scroll" data-target=".navbar-custom">
	
	
	
     <!-- Navigation -->
    <div id="navigation">
        <nav class="navbar navbar-custom" role="navigation">
                              <div class="container">
                                    <div class="row">
                                          <div class="col-md-2">
                                                   <div class="site-logo">
                                  <%
       String user = session.getAttribute("cui").toString();
       out.println("<a href='index.html' class='brand'>Bienvenido <br> CUI:" + user + " </a>");
    %>
                                                    </div>
                                          </div>
                                          

                                          <div class="col-md-10">
                         
                                                      <!-- Brand and toggle get grouped for better mobile display -->
                                          <div class="navbar-header">
                                                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#menu">
                                                <i class="fa fa-bars"></i>
                                                </button>
                                          </div>
                                                      <!-- Collect the nav links, forms, and other content for toggling -->
                                                      <div class="collapse navbar-collapse" id="menu">
                                                            <ul class="nav navbar-nav navbar-right">
                                                                  <li class="active"><a href="#intro">Home</a></li>
                                                                  <li><a href="#about">Voto</a></li>
																   
                                                                  <li><a href="#works">Works</a></li>				                                                                  
                                                                  
                                                                  <li><a href="#cambiar">Cambiar Contrasena</a></li>
                                                                  <li><a href="#canje">Canje Tarjeta</a></li>
                                                                  <li class="active"><a href="cerrarSesion">Salir</a></li>
                                                            </ul>
                                                      </div>
                                                      <!-- /.Navbar-collapse -->
                             
                                          </div>
                                    </div>
                              </div>
                              <!-- /.container -->
                        </nav>
    </div> 
    <!-- /Navigation -->  


	<!-- Section: about -->
	
    <section id="about" class="home-section color-dark bg-white">
     
<!-- 		<form action="uploader.php" method="post" enctype="multipart/form-data"> -->
<!--     Select image to upload: -->
<!--     <input type="file" name="fileToUpload" id="fileToUpload"> -->
<!--     <input type="submit" value="Upload Image" name="submit"> -->
<!-- </form> -->
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Votos por Dia</h2>
					<div class="divider-header"></div>
					<p>Las comidas mas votadas hoy Lunes 18 de Julio del 2016</p>
					</div>
					</div>
				</div>
			</div>

		</div>

		<div class="container">
<%-- <%BlobKey blobKey = new BlobKey(req.getParameter("blob-key"));%> --%>

		
        <div class="row">
	
			<div class="col-md-6">
			    <img src="img/dummy1.jpg" alt="" class="img-responsive" />
      
           </div>
			<%
		     
		     	
				if (session.getAttribute("1") != null){
					comida1 = (Integer) session.getAttribute("1");
					}
				if (session.getAttribute("2") != null){
					comida2 = (Integer) session.getAttribute("2");
					}
				if (session.getAttribute("3") != null) {
					comida3 = (Integer) session.getAttribute("3");
					}
				if (session.getAttribute("4") != null){
					comida4 = (Integer) session.getAttribute("4");
					}
 			String c1=(Integer) session.getAttribute("pp1")+"%";
 			String c2=(Integer) session.getAttribute("pp2")+"%";
			String c3=(Integer) session.getAttribute("pp3")+"%";
 			String c4=(Integer) session.getAttribute("pp4")+"%";
			%>
            <div class="col-md-6">		
			<p>Porcentaje de las comidas de hoy.</p>			
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-success" role="progressbar" aria-valuenow="40" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c1%>"><%=nab.getComida1()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-info" role="progressbafr" aria-valuenow="20" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c2%>"><%=nab.getComida2()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="60" 
			  aria-valuemin="0" aria-valuemax="100" style="width:<%=c3%>"><%=nab.getComida3()%>
			  </div>
			</div>
			<div class="progress progress-striped active">
			  <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="80" 
			  aria-valuemin="0" aria-valuemax="100" style="width: <%=c4%>"><%=nab.getComida4()%>
			  </div>
			</div>

            </div>
		

        </div>		
		</div>

	</section>
	<!-- /Section: about -->




<!-- Section: works -->								
    <section id="works" class="home-section color-dark text-center bg-white">
	<%	HttpSession misession = request.getSession();
	String img=(String)misession.getAttribute("imageUrl");
	%>
	<h2 class="h-bold">Comida sugerida para el dia martes</h2>
	<img src="<%=img%>" width=180; height=180 alt="Esperando actividad reciente de los administradores">
	
		<br><br><br><br>
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Comidas para Lunes 18 de Julio del 2016</h2>
					
					<div class="divider-header"></div>
					
					</div>
					</div>
					<p >Vote por la comida que mas le agrade.</p>
					<br><br>
					<p id="demo"></p>
					<form action="nutricionistaServlet" method="post">
					<button  name="candidato"value="1" type="submit" name="enviar"><img src="img/works/c1.jpg"  width=178 height=180 ></button>
					<button  name="candidato"value="2" type="submit" name="enviar"><img src="img/works/c2.jpg"  width=178 height=180 ></button>
					<button  name="candidato"value="3" type="submit" name="enviar"><img src="img/works/c3.jpg"  width=178 height=180 ></button>
 					<button  name="candidato"value="4" type="submit" name="enviar"><img src="img/works/c4.jpg"  width=178 height=180 ></button>
 					</form> 

				
 					<br><br>

<!-- 						<button onclick="myFunction5()">Ver</button> -->
							
				</div>
			</div>

		</div>
					


	</section>
	<!-- /Section: works -->




<!-- Section: cajnje -->
    <section id="canje" class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Canje de tarjeta</h2>
					<div class="divider-header"></div>
					<p></p>
					</div>
					</div>
				</div>
			</div>

		</div>
		
		<div class="container">

			<div class="row marginbot-80">
				<div class="col-md-8 col-md-offset-2">
						
						<form id="contact-form" class="wow bounceInUp" action="cambiarContra" method="post" data-wow-offset="10" data-wow-delay="0.2s">
						
						  
						
						<div class="row">
							<div class="col-md-12">
								
								 <ul class="nav navbar-nav navbar-right">
                                                            <li><a href="Doc/Carnet.docx" target='_blank' title="Click here to open a Word document">
                                                            <img src="Doc/comedorUnsa.jpg"  width=300 height=100 ></a></li>
																 
                                                        </ul>
							</div>
						</div>
						</form>
				</div>
			</div>	


		</div>
	</section>
	<!-- /Section: canje -->












		<!-- Section: cambiar contraseña -->
    <section id="cambiar" class="home-section nopadd-bot color-dark bg-gray text-center">
		<div class="container marginbot-50">
			<div class="row">
				<div class="col-lg-8 col-lg-offset-2">
					<div class="wow flipInY" data-wow-offset="0" data-wow-delay="0.4s">
					<div class="section-heading text-center">
					<h2 class="h-bold">Cambiar contrasena</h2>
					<div class="divider-header"></div>
					<p></p>
					</div>
					</div>
				</div>
			</div>

		</div>
		
		<div class="container">

			<div class="row marginbot-80">
				<div class="col-md-8 col-md-offset-2">
						
						<form id="contact-form" class="wow bounceInUp" action="cambiarContra" method="post" data-wow-offset="10" data-wow-delay="0.2s">
						
						  
						
						<div class="row">
							<div class="col-md-12">
								
								<div class="form-group">
										<input type="password" class="form-control input-lg" name="con1" id="con1" placeholder="contrasena" required="required" />
								</div>
								<div class="form-group">
										<input type="password" class="form-control input-lg" name="con2" id="con2" placeholder="nueva contrasena" required="required" />
								</div>						
								<button type="submit" class="btn btn-skin btn-lg btn-block" id="cambiar">
									Modificar Contrasena</button>
							</div>
						</div>
						</form>
				</div>
			</div>	


		</div>
	</section>
	<!-- /Section: cambiar comtraseña -->



	<footer>
		<div class="container">
			<div class="row">
				<div class="col-md-6 col-md-offset-3">
					
					<div class="text-center">
						<a href="#intro" class="totop"><i class="fa fa-angle-up fa-3x"></i></a>

						<p>Peru, Avenida Alfonso Ugarte, 121 Jesus Maria Paucarpata, Arequipa<br/>
						&copy;Copyright 2016 - Shuffle. Designed by <a href="http://facebook.com/mauricioMaldonado">Mauricio Maldonado</a></p>
                                              
					</div>
				</div>
			</div>	
		</div>
	</footer>



</body>

</html>
<%} %>