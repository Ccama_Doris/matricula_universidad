$(document).ready(function(){
	

	//Modal para agregar nuevo Usuario
	   		$("#agregarUsuario").click(function(){
	        $("#myModal").modal();
	   		 });

	//registrar usuario
			$('#registrar').click(function(event) {

				event.preventDefault();
				var varCui = $('#cui').val();
				var varDni = $('#dni').val();
				var varPat = $('#apaterno').val();
				var varName = $('#name').val();
				var varMat = $('#amaterno').val();
							
				$.get('registrarUsuario', {
					cui : varCui,
					dni : varDni,
					apaterno : varPat,
					amaterno : varMat,
					name : varName
					

				}, function(responseText) {
					alert(responseText);
					$("#myModal").modal("hide");
					update_table();
				});

			});


	// recover Modal Usuario
			$('.updUsuario').click(function(event){
		        var val=$(this).val();
		       	$.get('recoverUsuario',{
					clave:val
				}, function(responseText){		
					//div donde va a aparecer el modal
					$('#update_usuario').html(responseText);
					$("#myModalUpd").modal('show');

					//Update Usuario

					$('#updUs').click(function(event){	
						event.preventDefault();

					    var varIdUser = $('#idUser').val();
						var varCui = $('#cuiUpd').val();
						var varName = $('#nameUpd').val();
						var varPat = $('#apaternoUpd').val();
						var varMat = $('#amaternoUpd').val();
						var varDni = $('#dniUpd').val();
							
						$.get('updateUsuario',{
							cui : varCui,
							name : varName,		
						    apaterno : varPat,
							amaterno : varMat,
							dni : varDni,
							idUser : varIdUser
						}, function(responseText){		
							alert(responseText);
							$("#myModalUpd").modal("hide");
							update_table();
						});		
					});	
				});
				
			});

				
   		


	//Eliminar All Usuarios
			$('#deleteAllUsers').click(function(event) {
					$.get('deleteAllUsuarios', {
				
					}, function(responseText) {
						alert(responseText);
						update_table();
					});

				});










	//eliminar Pensionista
			$('#_eliminar').click(function(event) {

				var varIds = "";

				$("input[name='option[]']:checked").each(function() {
					varIds += ($(this).val()) + " ";
				});
				$.get('deletePensionista', {
					valores : varIds
				}, function(responseText) {

					$('#table_Pensionista').html(responseText);
				});

			});

	//Eliminar All Pensionistas
			$('#delAllPens').click(function(event) {
				$.get('deleteAllPensionistas', {
				
				}, function(responseText) {
					alert(responseText);
					
				});

			});	

		// recover Modal Pensionistas
			$('.updPensionista').click(function(event){
		        var val=$(this).val();
		       	$.get('recoverPensionista',{
					clave:val
				}, function(responseText){		
					//div donde va a aparecer el modal
					$('#update_pensionista').html(responseText);
					$("#myModalUpdP").modal('show');

					//Update Pensionista

					$('#updPe').click(function(event){	

					    var varIdPensionista = $('#idPensionista').val();
						var varEscuela = $('#escuelaUpd').val();
						var varEmail = $('#emailUpd').val();
						var varTelefono = $('#numberUpd').val();
						var varPass = $('#passUpd').val();
													
						$.get('updatePensionista',{
							escuelaUpd : varEscuela,
							emailUpd : varEmail,		
						    numberUpd : varTelefono,
							passUpd : varPass,
							
							idPensionista : varIdPensionista
						}, function(responseText){		
							alert(responseText);
							$("#myModalUpdP").modal("hide");
							$('#table_Pensionista').html(responseText);
							
						});		
					});	
				});
				
			});			



});


// funcion para borrar un USUARIO de la tabla
function deleteItem(cui) {
		$.get('deleteUsuario',{
			cuiDel : cui
		}, function(responseText){
			alert(responseText);
			update_table();
		});

	}



// funcion que carga de la mytable.jsp para mostrar solo la tabla al actualizar, borrar, crear un Usuario
function update_table(){
			//	get que llame al servlet mostrar usuario
				$.get('usuarioServlet', {
				}, function(responseText) {
					$('#tablaUsuario').html(responseText);
					
				});
			}
