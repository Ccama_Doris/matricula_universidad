							<div class="row marginbot-20">
								<div class="col-md-6">
									<div class="form-group">
											<label for="escuela"><span class="glyphicon glyphicon-tag"></span> Escuela</label>
											<select name="escuela" id="escuela" class="form-control input-lg">
											  <option value="Ingenieria de Sistemas">Ingenieria de Sistemas</option>
											  <option value="Ingenieria Industrial">Ingenieria Industrial</option>
											  <option value="Medicina">Medicina</option>
											  <option value="Derecho">Derecho</option>
											</select>
											
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
											<label for="e-mail"><span class="glyphicon glyphicon-envelope"></span> E-mail</label>
											<input type="email" class="form-control input-lg" name="email" id="email" placeholder="e-mail" required/>
									</div>
								</div>

							</div>
									<div class="row">
								<div class="col-md-6">
									<div class="form-group">
											<label for="telefono"><span class="glyphicon glyphicon-earphone"></span> Telefono</label>
											<input type="number" class="form-control input-lg" name="telefono" id="telefono" placeholder="Telefono"/>
									</div>
								</div>

								<div class="col-md-6">
									<div class="form-group">
											<label for="password"><span class="glyphicon glyphicon-eye-close"></span> Password</label>
											<input type="password" class="form-control input-lg" name="pass" id="pass" placeholder="Password maximo 8 caracteres" maxlength="8" required/>
									</div>
								</div>

							</div>
							<button type="submit" class="btn btn-skin btn-lg btn-block" id="registro">
							Crear Cuenta</button>	