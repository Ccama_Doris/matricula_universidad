package unsa.edu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class GuardarModProfesor extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		      
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Profesor.class);

		String DNI = req.getParameter("DNI");
		String Nombre = req.getParameter("Nombre");
		String ApellidoPaterno = req.getParameter("ApellidoPaterno");
		String ApellidoMaterno = req.getParameter("ApellidoMaterno");
		String FechaNacimiento = req.getParameter("FechaNacimiento");
		String Sexo = req.getParameter("Sexo");
		String Direccion = req.getParameter("Direccion");
		String Departamento = req.getParameter("Departamento");
		String Provincia = req.getParameter("Provincia");
		String Distrito = req.getParameter("Distrito");
		String Telefono = req.getParameter("Telefono");
		String GradoAcademico = req.getParameter("GradoAcademico");
		String DepartamentoAcademico = req.getParameter("DepartamentoAcademico");
		
		try {
		
			List<Profesor> pro=(List<Profesor>)q.execute();
			
			for(Profesor prof:pro){
				
				if(DNI.equals(prof.getDNI())){
					prof.setApellidoMaterno(ApellidoMaterno);
					prof.setApellidoPaterno(ApellidoPaterno);
					prof.setDepartamento(Departamento);
					prof.setDepartamentoAcademico(DepartamentoAcademico);
					prof.setDireccion(Direccion);
					prof.setDistrito(Distrito);
					prof.setDNI(DNI);
					prof.setFechaNacimiento(FechaNacimiento);
					prof.setGradoAcademico(GradoAcademico);
					prof.setNombre(Nombre);
					prof.setProvincia(Provincia);
					prof.setSexo(Sexo);
					prof.setTelefono(Telefono);
					
					
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/jsp/index.jsp");
					rd.forward(req, resp);
				}
			}
			
			
	
		} catch (Exception e) {
			resp.getWriter().print("ERROR");
		} finally {
			pm.close();
		}
	}
}