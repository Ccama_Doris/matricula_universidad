package unsa.edu;

import java.util.ArrayList;
import java.util.List;

import javax.jdo.annotations.IdentityType;
import javax.jdo.annotations.IdGeneratorStrategy;
import javax.jdo.annotations.PersistenceCapable;
import javax.jdo.annotations.Persistent;
import javax.jdo.annotations.PrimaryKey;
//import javax.jdo.annotations.Unique;



import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;
import com.google.appengine.datanucleus.annotations.Unowned;


@PersistenceCapable(identityType = IdentityType.APPLICATION)
public class Matricula {
	@PrimaryKey
	@Persistent(valueStrategy = IdGeneratorStrategy.IDENTITY)
	private Key idMatricula;
	@Persistent
	private String cui;
	@Persistent
	private String email;
	@Persistent
	@Unowned
	private List<Curso> curso = new ArrayList<Curso>();
	
	public Matricula(String cui, String email) {
		super();
		this.cui = cui;
		this.email = email;
	}
	
	public String getIdMatricula() {
		return KeyFactory.keyToString(idMatricula);
	}

	public void setIdMatricula(String idMatricula) {
		Key keyMatricula = KeyFactory.stringToKey(idMatricula);
		this.idMatricula = KeyFactory.createKey(keyMatricula,
		Matricula.class.getSimpleName(), java.util.UUID.randomUUID().toString());
	}

	public String getCui() {
		return this.cui;
	}

	public void setCui(String cui) {
		this.cui = cui;
	}

	public String getEmail() {
		return this.email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public List<Curso> getCurso() {
		return this.curso;
	}

	public void setCurso(List<Curso> curso) {
		this.curso = curso;
	}
	
}