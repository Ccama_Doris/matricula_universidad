package unsa.edu;

import java.io.IOException;
import javax.jdo.PersistenceManager;
import javax.servlet.http.*;

import com.google.appengine.api.datastore.Key;
import com.google.appengine.api.datastore.KeyFactory;

@SuppressWarnings("serial")
public class MatriculaCursos extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		resp.setContentType("text/plain");
		
		String cui = req.getParameter("cui");
		String email = req.getParameter("email");
		String[] curso = req.getParameterValues("curso");
				 
		final PersistenceManager pm = PMF.get().getPersistenceManager();		
		
		try{
			Key key;
			Curso found;
			Matricula p = new Matricula(cui, email);
			if( req.getParameterValues("curso")!=null )
				for(int i=0;i<curso.length;i++){
					System.out.println(curso[i]);
					key = KeyFactory.stringToKey(curso[i]);
					System.out.println("a3");
					found = pm.getObjectById(Curso.class, key);
					p.getCurso().add(found);
					System.out.println("a4");
				}
			try{
				pm.makePersistent(p);
				System.out.println("a");
				resp.getWriter().println("Matricula grabada correctamente.");
				resp.sendRedirect("/listmatricula");
				System.out.print(2);
			}catch(Exception e){
				System.out.println(e);
				resp.getWriter().println("Ocurrió un error, vuelva a intentarlo.");
				resp.sendRedirect("/Matricula");
			}
		}catch(Exception e){
			System.out.println(e);
		}finally{
			pm.close();
		}
		
	}
}
