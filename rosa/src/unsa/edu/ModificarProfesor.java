package unsa.edu;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Arrays;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.http.*;



@SuppressWarnings("serial")
public class ModificarProfesor extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Profesor.class);
		
		String dni = req.getParameter("DNI");
	
		q.setFilter("dni == dniParam");
		q.declareParameters("String dniParam");
		
		try{
			System.out.println("x1"+dni);
			List<Profesor> prof = (List<Profesor>) q.execute(dni);
			System.out.println("x2");

			for(Profesor p: prof){
				System.out.println("x4");

				 if(dni.equals(p.getDNI()))
			        {
						req.setAttribute("profesor", prof);
			            RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/jsp/modificarProfesor.jsp");
						rd.forward(req, resp);
			        }
				 
			}
			RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/jsp/buscarProfesor.jsp");
			rd.forward(req, resp);
		}catch(Exception e){
			resp.getWriter().print("Ups .. ERROR");
		}finally{
			 q.closeAll();
		}
	}
}
