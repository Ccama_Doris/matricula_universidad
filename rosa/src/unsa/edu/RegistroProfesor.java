package unsa.edu;

import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Map;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.*;

@SuppressWarnings("serial")
public class RegistroProfesor extends HttpServlet {
	public void doPost(HttpServletRequest req, HttpServletResponse resp)
			throws IOException {
		      
		PersistenceManager pm = PMF.get().getPersistenceManager();
		Query q = pm.newQuery(Profesor.class);

		String DNI = req.getParameter("DNI");
		String Nombre = req.getParameter("Nombre");
		String ApellidoPaterno = req.getParameter("ApellidoPaterno");
		String ApellidoMaterno = req.getParameter("ApellidoMaterno");
		String FechaNacimiento = req.getParameter("FechaNacimiento");
		String Sexo = req.getParameter("Sexo");
		String Direccion = req.getParameter("Direccion");
		String Departamento = req.getParameter("Departamento");
		String Provincia = req.getParameter("Provincia");
		String Distrito = req.getParameter("Distrito");
		String Telefono = req.getParameter("Telefono");
		String GradoAcademico = req.getParameter("GradoAcademico");
		String DepartamentoAcademico = req.getParameter("DepartamentoAcademico");
		
		try {
			Profesor profe = new Profesor(DNI,  Nombre, ApellidoPaterno,  ApellidoMaterno, FechaNacimiento, Sexo, 
					 Direccion, Departamento, Provincia, Distrito,  Telefono,
					 GradoAcademico,  DepartamentoAcademico);
			List<Profesor> pro=(List<Profesor>)q.execute();
			int cont=0;

			for(Profesor prof:pro){
				System.out.println(2);
				//cont=0;
				if(DNI.equals(prof.getDNI())){
					System.out.println(3);
					cont++;
					break;
				}
			}
			System.out.println("cont"+cont);
			if(cont==0){
				System.out.println(4);
				pm.makePersistent(profe);
			}
			else{
				resp.getWriter().print("ESTE PROFESOR YA SE ENCUENTRA REGISTRADO");
			}
	
		} catch (Exception e) {
			resp.getWriter().print("ERROR");
		} finally {
			pm.close();
		}
	}
}