package unsa.edu;

import java.io.IOException;
import java.util.List;

import javax.jdo.PersistenceManager;
import javax.jdo.Query;
import javax.servlet.RequestDispatcher;
import javax.servlet.ServletException;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

	@SuppressWarnings("serial")
	public class listmatricula extends HttpServlet{
		@Override
		protected void doGet(HttpServletRequest req, HttpServletResponse resp)
				throws ServletException, IOException {
			//resp.setContentType("text/plain");
			
			final PersistenceManager pm = PMF.get().getPersistenceManager();
			final Query q = pm.newQuery(Matricula.class);

			
			if( req.getParameter("curso")!=null ){
				String curso = req.getParameter("curso");
				//q.setOrdering("idPersona ascending");
				q.setOrdering("idMatricula descending");
				//q.setRange(0, 10);
				q.setFilter("curso.getNomcurso() == cursoParam");
				q.declareParameters("String cursoParam");
				try{
					System.out.println("x");
					@SuppressWarnings("unchecked")
					List<Matricula> matricula = (List<Matricula>) q.execute(curso);
					System.out.println("H");

					req.setAttribute("matricula", matricula);
					System.out.println("HOLA");
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/jsp/listmatricula.jsp");
					rd.forward(req, resp);
					System.out.println("a");
				
				}catch(Exception e){
					System.out.println(e);
				}finally{
					q.closeAll();
					pm.close();
				}
				
			}else {
				q.setOrdering("idMatricula descending");
				//q.setRange(0, 10);	 
				try{
					@SuppressWarnings("unchecked")
					
					List<Matricula> matricula = (List<Matricula>) q.execute();
					req.setAttribute("matricula", matricula);
					RequestDispatcher rd = req.getRequestDispatcher("/WEB-INF/jsp/listmatricula.jsp");
					rd.forward(req, resp);
				}catch(Exception e){
					System.out.println(e);
				}finally{
					q.closeAll();
					pm.close();
				}
			}			
		}
	}

	
